jsToolBar.prototype.elements.comment_answer={
	type:'button',
	title:'comment_answer',
	icon:'index.php?pf=comment_answer/comment_answer.png',
	fn:{},
	prompt:{}
};

jsToolBar.prototype.elements.comment_answer.fn.xhtml=function(){
	this.encloseSelection('','',function(str){
		if(str == "")
			str="Texte";
		return "<p><span style=\"color: rgb(145, 111, 138);\">Réponse : "+str+"</span></p>";
	});
};

/*
jsToolBar.prototype.elements.comment_answer.fn.wiki=function(){
	this.encloseSelection('','',function(str){
		alert(str);
		return "<p style=\"color: rgb(145, 111, 138);\">Réponse : "+str+"</p>";
	});
};
*/

jsToolBar.prototype.elements.comment_answer.fn.wysiwyg=function(){
	var text_fragment = this.getSelectedNode();

	var span_new = this.iwin.document.createElement("span");
	span_new.style.color="rgb(145, 111, 138)";
	if(text_fragment.textContent == "")
		span_new.innerHTML="Réponse : Texte";
	else
		span_new.innerHTML="Réponse : ";
	span_new.appendChild(text_fragment);
	this.insertNode(span_new);
};
